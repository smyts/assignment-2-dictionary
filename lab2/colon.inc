%define pointer 0

%macro colon 2
        %ifstr %1
                %ifid %2
                        %%localpointer:
                        dq pointer

                        %define pointer %%localpointer

                        db %1, 0
                        %2:
                %else
                        %error "second argument must be id"
                %endif
        %else
                %error "first argument must be string"
        %endif
%endmacro
