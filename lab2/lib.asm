%define space_code 32
%define newline_code 10
%define tabbulation_code 9

section .text

global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy


; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, 60
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
.loop:                  ; main loop starts here
  cmp byte [rdi+rax], 0 ; Check if the current symbol is null-terminator.
                        ; We absolutely need that 'byte' modifier since
                        ; the left and the right part of cmp should be
                        ; of the same size. Right operand is immediate
                        ; and holds no information about its size,
                        ; hence we don't know how many bytes should be
                        ; taken from memory and compared
  je .end               ; Jump if we found null-terminator
  inc rax               ; Otherwise go to next symbol and increase
                        ; counter
  jmp .loop
.end:
  ret                   ; When we hit 'ret', rax should hold return value

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
	xor rax, rax

	call string_length
	mov rsi, rdi
	mov rdx, rax
	mov rax, 1
	mov rdi, 1
	syscall

	ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    xor rax, rax
    mov rdi, 0xA


; Принимает код символа и выводит его в stdout
print_char:
    xor rax, rax

    push rdi
    mov rax, 1
    mov rdi, 1
    mov rdx, 1
    mov rsi, rsp
    syscall
    
    pop rdi
    
    ret


; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.

print_uint:
        xor rax, rax
        mov rcx, rsp

        mov rax, rdi
        mov rsi, 10
        push 0
.loop:
        xor rdx, rdx

        div rsi
        dec rsp
        mov byte[rsp], dl
        add byte[rsp], 48

        cmp rax, 0
        jne .loop

.out:
        mov rdi, rsp
        push rcx
        call print_string
        pop rcx
        mov rsp, rcx
        ret

; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
	test rdi, rdi
	jns print_uint
        xor rax, rax

        push rdi
        mov rdi, '-'
        call print_char
        pop rdi
        neg rdi
        jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
        xor rax, rax

        push r12
        push r13

        call string_length
        mov r12, rax
        mov rax, rdi
        mov rdi, rsi
        mov rsi, rax
        call string_length
        mov r13, rax
        cmp r12, r13
        jne .end_not_equals

.loop:
        mov al, byte[rdi]
        mov bl, byte[rsi]
        cmp al, bl
        jne .end_not_equals

        cmp al, 0
        je .end_equals
        inc rsi
        inc rdi
        jmp .loop

.end_not_equals:
        mov rax, 0

        pop r13
        pop r12

        ret
.end_equals:
        mov rax, 1

        pop r13
        pop r12

        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:

        xor rax, rax
        push 0

        xor rax, rax
        xor rdi, rdi
        mov rsi, rsp
        mov rdx, 1
        syscall

        pop rax

        ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
;rdi хранит адрес начала
;rsi хранит размер

        xor rdx, rdx
	xor rcx, rcx
        cmp rsi, 0
        je .end_error


.begin:                 ;looping while spaces

	push rsi
	push rdi
	push rcx

        call read_char

	pop rcx
	pop rdi
	pop rsi

        cmp al, newline_code
        je .begin
        cmp al, space_code
        je .begin
        cmp al, tabbulation_code
        je .begin
        cmp al, 0
        je .end_error

.main_loop:

        cmp rsi, rcx
        je .end_error

        mov byte[rdi + rcx], al
        inc rcx

	push rsi
	push rdi
	push rcx

        call read_char

	pop rcx
	pop rdi
	pop rsi

        cmp al, newline_code
        je .end
        cmp al, space_code
        je .end
        cmp al, tabbulation_code
        je .end
        cmp al, 0
        je .end

        jmp .main_loop

.end:
        ;inc rcx
        mov byte[rdi + rcx], 0
        mov rax, rdi
        mov rdx, rcx

        ret

.end_error:
        mov rax, 0
        mov rdx, rcx

        ret



; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
        ;rdi  указатель на строку
        ;rax result
        ;rcx counter

        xor rax, rax
        mov rcx, 0
        mov rdx, 0

.loop:
        mov dl, byte[rdi + rcx]
        sub dl, 48
        cmp dl, 0
        js .out
        cmp dl, 10
        jns .out
        imul rax, 10
        add rax, rdx
        inc rcx
        jmp .loop

        ret

.out:
        mov rdx, rcx
        ret



; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
        ;rdi <- указатель на строку
        ;rax result
        ;rcx counter

        xor rax, rax
        mov rcx, 0
        mov rdx, 0

        mov dl, byte[rdi]
        cmp dl, 45
        je .signed
        jmp parse_uint

.signed:
        inc rdi
        call parse_uint
        neg rax
        inc rdx
        ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
        xor rax, rax
        mov rcx, 0

.loop:
        cmp rdx, 0
        je .out_error
        mov r8b, byte[rdi + rcx]
        mov byte[rsi + rcx], r8b
        cmp byte[rdi + rcx], 0
        je .out
        dec rdx
        inc rcx
        jmp .loop

.out_error:
        mov rax, 0
        ret

.out:
        mov rax, rcx

        ret

