section .text

extern string_equals
extern string_length
global find_word

find_word:
        ;rdi string pointer
        ;rsi dictionary pointer

	test rsi, rsi
	je .not_found

.loop:
        push rsi
        push rdi
        add rsi, 8

        call string_equals

        pop rdi
        pop rsi

        cmp rax, 1
        je .found

        mov rsi, [rsi]
        cmp rsi, 0
        je .not_found
        jmp .loop

.found:
        mov rax, rsi
        ret

.not_found:
        xor rax, rax
        ret
