section .rodata

long_word_error_message:
db 'word length should be less than 256',0

word_not_found_error_message:
db 'there is no such word in dictionary',0

section .data

%include "lib.inc"
%include "words.inc"
%define STACK_BUFFER_SIZE 256

global _start
extern find_word

section .text

print_error:
        push rdi
        call string_length
        pop rdi
        mov rdx, rax
        mov rsi, rdi
        mov rax, 1
        mov rdi, 2
        syscall
        ret

_start:
        sub rsp, STACK_BUFFER_SIZE
        mov rdi, rsp
        mov rsi, STACK_BUFFER_SIZE
        call read_word

        cmp rax, 0
        je .long_word_error

        mov rdi, rax
        mov rsi, pointer
        call find_word

        cmp rax, 0
        je .word_not_found_error

        add rax, 8
        mov rdi, rax
        call string_length
        inc rax
        add rdi, rax
        call print_string
        call print_newline
        call exit

.long_word_error:
        mov rdi, long_word_error_message
        call print_error
	call print_newline
	call exit

.word_not_found_error:
        mov rdi, word_not_found_error_message
        call print_error
	call print_newline
	call exit
